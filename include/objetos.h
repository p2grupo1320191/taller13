//En este archivo ud podra definir los objetos que podrán ser usados 
//con el slab allocator. Declare tambien el constructor y destructor de 
//cada objeto declarado. Abajo se provee un objeto de ejemplo.
//Declare al menos tres objetos distintos

//El constructor siempre recibe un puntero tipo void al tipo de objeto,
//y su tamaño (tal como se declaro el puntero a funcion).
//No olvide implementar las funciones de crear y destruir de cada objeto.

/*
typedef struct ejemplo{
    int a;
    float b[100];
    char *msg;
    unsigned int refcount;
} Ejemplo; 
*/

typedef struct juego{
	char *nombre;
	char *descripcion;	
	int niveles;
	char *clasificacion;
	float puntaje;
} Juego;

typedef struct documento{
	char *nombre;
	char *descripcion;
	char *extension;
	char *fecha_modif;
} Documento;

typedef struct programa{
	char *nombre;
	char *descripcion;
	char *funcionalidad;
} Programa;

typedef struct objetoEjemplo{
	int id_hilo;
	int *data; //arreglo de 3 enteros
} objeto_ejemplo;

//Constructores
//void crear_Ejemplo(void *ref, size_t tamano);
void crear_Juego(void *ref, size_t tamano);
void crear_Documento(void *ref, size_t tamano);
void crear_Programa(void *ref, size_t tamano);
void crear_objeto_ejemplo(void *ref, size_t tamano);

//Destructores
//void destruir_Ejemplo(void *ref, size_t tamano);
void destruir_Juego(void *ref, size_t tamano);
void destruir_Documento(void *ref, size_t tamano);
void destruir_Programa(void *ref, size_t tamano);
void destruir_objeto_ejemplo(void *ref, size_t tamano);
//TODO: Crear 3 objetos mas
