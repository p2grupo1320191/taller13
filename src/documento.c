#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_Documento(void *ref, size_t tam){
	Documento *doc=(Documento *)ref;
	doc->nombre = (char*)calloc(15, sizeof(char));
	doc->descripcion = (char*)calloc(50, sizeof(char));
	doc->extension = (char*)calloc(5, sizeof(char));
	doc->fecha_modif = (char*)calloc(10, sizeof(char));
}

void destruir_Documento(void *ref, size_t tam){
	Documento  *doc = (Documento *)ref;
	free(doc->nombre);
	free(doc->descripcion);
	free(doc->extension);
	free(doc->fecha_modif);
}
