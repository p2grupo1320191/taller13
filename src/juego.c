#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_Juego(void *ref, size_t tam){
        Juego *jue=(Juego *)ref;
        jue->nombre = (char*)calloc(15, sizeof(char));
        jue->descripcion = (char*)calloc(50, sizeof(char));
        jue->niveles = 0;
        jue->clasificacion = (char *)calloc(20,sizeof(char));
        jue->puntaje = 0.0f;
}

void destruir_Juego(void *ref, size_t tam){
        Juego  *jue = (Juego *)ref;
        free(jue->nombre);
        free(jue->descripcion);
        jue->niveles = -1;
        free(jue->clasificacion);
        jue->puntaje = -1;
}

