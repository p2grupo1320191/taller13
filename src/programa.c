
#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_Programa(void *ref, size_t tam){
        Programa *prog=(Programa *)ref;        
        prog->nombre = (char *)calloc(15, sizeof(char));
        prog->descripcion = (char *)calloc(50, sizeof(char));
        prog->funcionalidad = (char *)calloc(20, sizeof(char));
}

void destruir_Programa(void *ref, size_t tam){
        Programa  *prog = (Programa *)ref;        
        free(prog ->nombre);
        free(prog ->descripcion);
        free(prog ->funcionalidad);
}


