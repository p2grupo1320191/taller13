#include "slaballoc.h"
#include "objetos.h"



SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){

    //Se va creando la cache asignando cada dato de nuestro slaballoc con los argumentos que mandamos a la funcion

    SlabAlloc *slab=(SlabAlloc *)malloc(sizeof(SlabAlloc));

    slab->nombre=nombre;

    slab->mem_ptr=(void *)malloc(tamano_objeto*TAMANO_CACHE);

    slab->tamano_cache=TAMANO_CACHE;

    slab->constructor=constructor;

    slab->destructor=destructor;
    
    int c1=0;
    
    while(c1<tamano_objeto*TAMANO_CACHE){
    
      constructor(slab->mem_ptr+c1,tamano_objeto);
      c1 += tamano_objeto;

    }

    
    //Se inicializan los demas valores
    slab->cantidad_en_uso=0;

    slab->tamano_objeto=tamano_objeto;

    Slab* punteroSlab=(Slab *)malloc(sizeof(Slab)*TAMANO_CACHE);

    
    int c2=0;
    
    while(c2<TAMANO_CACHE){    

      Slab cache;


      cache.ptr_data = slab->mem_ptr+(c2*tamano_objeto);

      cache.ptr_data_old = slab->mem_ptr+(c2*tamano_objeto);

      cache.status = DISPONIBLE;


      *(punteroSlab+c2)=cache;
      
      c2++;

    }

    slab->slab_ptr=punteroSlab;
    return slab;

}

void *obtener_cache(SlabAlloc *alloc, int crecer){

  if(alloc == NULL){

    return NULL;

  }


  Slab *ptr = alloc->slab_ptr;


  int c = 0;

  //P(s)

  while(c< alloc->tamano_cache){

    if(ptr[c].status == DISPONIBLE){

      ptr[c].status = EN_USO;

      alloc->cantidad_en_uso +=1;

      return ptr[c].ptr_data;
    }
    c++;

  }

  //V(s)

  return NULL;

}

void devolver_cache(SlabAlloc *alloc, void *obj){

  Slab *slab= alloc->slab_ptr;
  
  int c = 0;
  
  while (c<alloc->tamano_cache){  

    if (obj==slab[c].ptr_data){

      slab[c].ptr_data=NULL;

      slab[c].status= DISPONIBLE;

      alloc->cantidad_en_uso -=1;

      alloc->destructor(alloc->mem_ptr + alloc->tamano_objeto*c, alloc->tamano_objeto);

      break;

    }
    c++;

  }

}

void destruir_cache(SlabAlloc *cache){

  if (cache->cantidad_en_uso == 0){

    int c = 0;
    
    while(c<cache->tamano_cache) { 

      cache->destructor(alloc->mem_ptr + alloc->tamano_objeto*c, alloc->tamano_objeto);
      c++;
        
    }

  free(cache->slab_ptr);

  free(cache->mem_ptr);

  cache = NULL;

  free(cache);

  }

}

void stats_cache(SlabAlloc *cache){
  SlabAlloc micache = *cache;

  printf("\nNombre del SlabAlloc:\t%s\n", micache.nombre);

  printf("Cantidad de slabs:\t%d\n", micache.tamano_cache);

  printf("Cantidad de slabs en uso:\t%d\n", micache.cantidad_en_uso);

  printf("Tamano de objeto:\t%ld\n\n", micache.tamano_objeto);

  printf("Dirección de cache: %p\n\n",micache.mem_ptr);


  Slab *slab = cache->slab_ptr;
  
  int c=0;
  
  while(c<TAMANO_CACHE){  

    if(slab[c].status == DISPONIBLE){

      printf("Direccion ptr[%d].ptr_data: <%p>    <DISPONIBLE>,   ptr[%d].ptr_data_old: <%p>\n", c, slab[c].ptr_data, c, slab[c].ptr_data_old);

    } else {

      printf("Direccion ptr[%d].ptr_data: <%p>    <EN_USO>,   ptr[%d].ptr_data_old: <%p>\n", c, slab[c].ptr_data, c, slab[c].ptr_data_old);

    }
    c++;

  }

}
